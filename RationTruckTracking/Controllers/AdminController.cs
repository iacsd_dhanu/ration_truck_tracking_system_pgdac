﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BOL;
using BLL;

namespace RationTruckTracking.Controllers
{
    public class AdminController : ApiController
    {
        public List<Admin> Get()
        {
            List<Admin> adminList = AdminBM.GetAllAdmin();
            Console.WriteLine(adminList);
            return adminList;


        }
        public Admin Get(String email, string empId)
        {

            return AdminBM.GetAdminByLogin(email,empId);

        }
        public void Post(Admin newAdmin)
        {
            AdminBM.InsertAdmin(newAdmin);
        }
       [HttpDelete]
        public void Delete(int adminId)
        {
            AdminBM.DeleteAdmin(adminId);
        }

    }
}