﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BOL;
using BLL;
namespace RationTruckTracking.Controllers
{
    
    public class CustomersController : Controller
    {
        // GET api/<controller>
        public ActionResult Form()
        {
            

            return View();
        }
        [HttpGet]
        public ActionResult Index()
        {
            List<Customers> clist = CustomerBM.GetAllCustomers();

            return View();
        }
        [HttpPost]
        public ActionResult Login(string uname, string rationNo)
        {
            Response.Write(uname);
            Response.Write(rationNo);
            Customers customer =CustomerBM.GetCustomerByLogin(uname, rationNo);
          //  ViewData["customer"] = customer;
        
            Response.Write(customer.customerContactNumber);
         


            return View();
        }
        [HttpPost]
        public ActionResult Insert(Customers newCustomer)
        {
            CustomerBM.InsertCustomer(newCustomer);
            return View();
        }
        public ActionResult Put(Customers exitsCustomer)
        {
            CustomerBM.UpdateCustomer(exitsCustomer);
            return View();
        }
        [HttpGet]
        public ActionResult Delete(int custId)
        {
            CustomerBM.DeleteCutsomer(custId);
            return View();
        }

    }
}