﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BOL;
using BLL;
namespace RationTruckTracking.Controllers
{
    public class DistributorsController : ApiController
    {
        public List<Distributors> Get()
        {
            List < Distributors > distributorsList = DistributorsBM.GetAllDistributors();

            return distributorsList;


        }
        public Distributors Get(String uname, long contactNum)
        {

            return DistributorsBM.GetDistributorByLogin(uname, contactNum);
        
        }
        public void Post(Distributors newDistributor)
        {
             DistributorsBM.InsertDistributor(newDistributor);
        }
        public void Put(Distributors exitsDistributor)
        {
            DistributorsBM.UpdateDistributor(exitsDistributor);
        }
        [HttpDelete]
        public void Delete(int distId)
        {
            DistributorsBM.DeleteDistributor(distId);
        }

    }
}
