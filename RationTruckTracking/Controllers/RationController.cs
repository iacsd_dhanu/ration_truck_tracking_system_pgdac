﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BOL;
using BLL;
namespace RationTruckTracking.Controllers
{
    public class RationController : ApiController
    {
        public List<Ration> Get()
        {
            List<Ration> RationList = RationBM.GetAllRation();

            return RationList;


        }

        public void Post(Ration newGoods)
        {
            RationBM.InsertRation(newGoods);
        }
        public void Put(Ration oldRation)
        {
            RationBM.UpdateRation(oldRation);
        }
        public void Delete(int rationId)
        {
            RationBM.DeleteRation(rationId);
        }
    }
}