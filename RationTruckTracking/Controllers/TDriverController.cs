﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BOL;
using BLL;
namespace RationTruckTracking.Controllers
{
    public class TDriverController : ApiController
    {
        public List<TruckDriverDetails> Get()
        {
            List<TruckDriverDetails> deriverDetails=TruckDriverDetailsBM.GetAllTruckDriverDetails();

            return deriverDetails;


        }
      
        public void Post(TruckDriverDetails newDriver)
        {
            TruckDriverDetailsBM.InsertTruckDriverDetails(newDriver);
        }
        public void Put(TruckDriverDetails oldDriver)
        {
            TruckDriverDetailsBM.UpdateTruckDriverDetails(oldDriver);
        }
        public void Delete(int driverId)
        {
           TruckDriverDetailsBM.DeleteTruckDriver(driverId);
        }
    }
}