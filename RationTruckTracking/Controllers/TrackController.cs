﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BOL;
using BLL;
namespace RationTruckTracking.Controllers
{
    public class TrackController : ApiController
    {
        public List<TrackingDetails> Get()
        {
            List<TrackingDetails> trackDetails = TrackingBM.GetAllTrackingDetails();

            return trackDetails;


        }

        public void Post(TrackingDetails newTrack)
        {
           // TrackingBM.InsertTrackingDetails(newTrack);
        }
        [HttpPut]
        public void Put(string currentMessage, string date1, string date2, string stat, int id)
        {
            TrackingBM.UpdateTrackingDetails(currentMessage, date1,
                                                    date2, stat,
                                                     id);
        }
        [HttpDelete]
        public void Delete(int trackId)
        {
            TrackingBM.DeleteTrack(trackId);
        }
    }
}